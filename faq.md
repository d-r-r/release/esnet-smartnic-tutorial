# Frequently Asked Questions


## Terminal Configuration

If you encounter the following warning when using rlwrap:

```
rlwrap: warning: your $TERM is 'xterm-256color' but rlwrap couldn't find it in the terminfo database. Expect some problems.
```

You can resolve this issue by adding the following line to your .profile file:

```
export TERM=xterm1
```


## Failed bring-up of firmware container

If the "Running the firmware" step in the [execution workflow](wf-exec.md) fails with the following message (where the *X* characters are don't-cares):

```
esnet-smartnic-fw/sn-stack$ docker compose up -d
[+] Running 9/9
Error response from daemon: driver failed programming external connectivity on endpoint sn-stack-XXXXXXXX-xilinx-hwserver-1 (XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX): Bind for 0.0.0.0:3121 failed: port is already allocated
```

Then it's likely that you already have a version of the system running -- this can be confirmed by listing running containers:

```
esnet-smartnic-fw/sn-stack$ docker ps | more
CONTAINER ID   IMAGE                                COMMAND                  CREATED          STATUS                  PORTS                                       NAMES
8a535e073574   xilinx-labtools-docker:XXXXXXX-dev   "/entrypoint.sh /bin…"   13 days ago      Up 13 hours (healthy)   0.0.0.0:3121->3121/tcp, :::3121->3121/tcp   sn-stack-XXXXXXX-xilinx-hwserver-1
```


## MAC-level counters

Expanding on the description in the [execution workflow](wf-exec.md), detailed MAC-level stats are obtained as follows:

```
root@smartnic-fw:/# sn-cli cmac stats
CMAC0
  Tx:
             0 pkts         0 bytes   ok
             0 pkts         0 bytes   errors
             0 pkts  large
             0 pkts  small
             0 pkts  bad fcs
             0 pkts  unicast
             0 pkts  multicast
             0 pkts  broadcast
             0 pkts  vlan
  Rx:
             0 pkts         0 bytes   ok
             0 pkts         0 bytes   errors
             0 pkts  large
             0 pkts  small
             0 pkts  undersize
             0 pkts  fragment
             0 pkts  oversize
             0 pkts  too long
             0 pkts  jabber
             0 pkts  bad fcs
             0 pkts  stomped fcs
             0 pkts  unicast
             0 pkts  multicast
             0 pkts  broadcast
             0 pkts  vlan
             0 pkts  truncated
CMAC1
  Tx:
             0 pkts         0 bytes   ok
             0 pkts         0 bytes   errors
             0 pkts  large
             0 pkts  small
             0 pkts  bad fcs
             0 pkts  unicast
             0 pkts  multicast
             0 pkts  broadcast
             0 pkts  vlan
  Rx:
             0 pkts         0 bytes   ok
             0 pkts         0 bytes   errors
             0 pkts  large
             0 pkts  small
             0 pkts  undersize
             0 pkts  fragment
             0 pkts  oversize
             0 pkts  too long
             0 pkts  jabber
             0 pkts  bad fcs
             0 pkts  stomped fcs
             0 pkts  unicast
             0 pkts  multicast
             0 pkts  broadcast
             0 pkts  vlan
             0 pkts  truncated
```


## Encountering a new problem

Because of how the toolchain is set up, a process crashing in one container can affect a relying process in another container.
If you can reproduce a new problem, then inspect the output of `docker compose exec smartnic-fw sn-cli dev version` and `docker compose ps` and `docker compose logs`, and the contents of your ".env" file. This can give you clues about what originally might have failed and cascaded into the output you observed.
