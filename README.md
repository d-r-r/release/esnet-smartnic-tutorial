# Using the ESnet SmartNIC Framework

This documentation was prepared for the course [Applications of Programmable Networking (CS595)](http://www.cs.iit.edu/~nsultana1/teaching/F23CS595/) which ran during Fall 2023 at Illinois Institute of Technology.
It combines -- and marginally extends -- documentation that was prepared by ESnet for their SmartNIC platform to guide you through the process of developing, configuring, deploying and executing Smart NIC applications on AMD-Xilinx Alveo cards.


## Cookbook

The ESnet SmartNIC is an open-source framework to program AMD-Xilinx Alveo FPGA cards. In this cookbook we will use this framework to program these cards using the P4 language.
We divide the process into four workflows:

- The [development workflow](wf-dev.md) involves compiling your P4 programs into bitfiles.

- The [deployment workflow](wf-deploy.md) involves loading your bitfile onto a card.

- The [execution workflow](wf-exec.md) involves starting the program that has been loaded onto a card.

- The [configuration workflow](wf-config.md) involves configuring the host where the card is installed.

- [FAQ](FAQ.md)


## References

- [ESnet SmartNIC block diagram](https://github.com/esnet/esnet-smartnic-hw/blob/6b1bf47fea8a87191b9e1fcca8b4121181635c84/docs/SmartNIC_Block_Diagram.png)
- [Vitis Networking P4 Installation Guide and Release Notes (UG1307)](https://docs.xilinx.com/r/2023.1-English/ug1307-vitis-p4-install/Release-Notes-and-Known-Issues)
- [Vitis Networking P4 User Guide (UG1308)](https://docs.xilinx.com/r/en-US/ug1308-vitis-p4-user-guide)
- [Vitis Networking P4 Getting Started Guide (UG1373)](https://docs.xilinx.com/r/en-US/ug1373-vitis-p4-getting-started/Introduction)


## Prerequisites

If you are a student taking CS595 at IIT in Fall 2023, then Airfield would be your pre-configured system. Everything mentioned in this documentation has been tested on Airfield.

Otherwise, start with the [configuration workflow](wf-config.md) to setup up your host(s) for development and testing.


## Dependency and Version information

We will rely on the following:

- [Ubuntu 20.04](https://www.releases.ubuntu.com/focal/) with Linux 5.4.0-153.
- [Vivado 2023.1](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2023-1.html)
- The following IPs: (1) "LogiCORE, Vitis Networking P4 Evaluation License", (2) "LogiCORE, BCAM, STCAM (LPM), TCAM (All) Evaluation License", (3) "UltraScale+ Integrated 100G Ethernet No Charge License". You won't see item (3) listed directly in the licensing menu, but this link explains how to include it in the license: [CMAC license](https://github.com/esnet/open-nic-shell#cmac-license).
- The [esnet-smartnic-hw](https://github.com/esnet/esnet-smartnic-hw) repository (commit: [9ee2cbb](https://github.com/esnet/esnet-smartnic-hw/tree/9ee2cbb2e400a5b41484ff1a35bd2a3b9bc1e155)).
- The [esnet-smartnic-fw](https://github.com/esnet/esnet-smartnic-fw) repository (commit: [180595c](https://github.com/esnet/esnet-smartnic-fw/tree/579a23b7f55c289d241ab8ee6dc00dcdb1078f51)).
- The [smartnic-dpdk-docker](https://github.com/esnet/smartnic-dpdk-docker) repository (commit: [a52dba3](https://github.com/esnet/smartnic-dpdk-docker/tree/a52dba351cce4ff27b323339d27179cc334e5651)).
- The [xilinx-labtools-docker](https://github.com/esnet/xilinx-labtools-docker) repository (commit: [87f55f2](https://github.com/esnet/xilinx-labtools-docker/tree/87f55f2beb31fef639c0847ab30dee8ecfcff42e)).

**Note:** The evaluation status of one of the IPs that is used above results in a 48-hour validity period for artefacts.


## Copyright and License

This documentation consists of [CC BY 4.0](LICENSE)-licensed modifications that are derived from [separately-licensed earlier work](LICENSE_SOURCE.md).
